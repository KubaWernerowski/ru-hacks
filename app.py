
from flask import Flask, render_template
import plotly
import plotly.graph_objs as go
import json
import numpy as np
import pandas as pd
import pickle


app = Flask(__name__)
ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

pickle_in = open('data.pickle', 'rb')
stats = pickle.load(pickle_in)


@app.route('/')
def index():
    min_letter, max_letter, min_val, max_val = min_max_non_zero(stats)
    bar = create_plot()
    return render_template('index.html', plot=bar, min_letter=min_letter,
                           max_letter=max_letter, min_val=min_val, max_val=max_val)


def avg(nums):
    if len(nums) == 0:
        return 0
    else:
        return sum(nums) / len(nums)


def min_max_non_zero(data):
    min_val = 0
    min_letter = ''
    max_val = max([avg(data[letter]) for letter in ALPHABET])
    print(max_val)
    max_letter = ''

    for letter in ALPHABET:
        if min_val == 0 and avg(data[letter]) != 0:
            min_val = avg(data[letter])
            min_letter = letter
        if avg(data[letter]) < min_val:
            min_val = avg(data[letter])
            min_letter = letter
        if avg(data[letter]) == max_val:
            max_letter = letter

    return min_letter, max_letter, round(min_val, 2), round(max_val, 2)


def create_plot():

    x = np.array(list(ALPHABET))
    y = np.array([avg(stats[letter]) for letter in ALPHABET])
    df = pd.DataFrame({'x': x, 'y': y})

    data = [
        go.Bar(
            x=df['x'],
            y=df['y']
        )
    ]

    graph_JSON = json.dumps(data, cls=plotly.utils.PlotlyJSONEncoder)

    return graph_JSON


if __name__ == '__main__':
    app.run()
